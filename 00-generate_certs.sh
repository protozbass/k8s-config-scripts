#!/usr/bin/bash -e

function generate_ca_config() {
  echo -e "Generating CA Certificate"
  cat > ca-config.json << EOF
{
  "signing": {
    "default": {
      "expiry": "8760h"
    },
    "profiles": {
      "kubernetes": {
        "usages": [
          "signing", 
          "key encipherment", 
          "server auth", 
          "client auth"
          ],
        "expiry": "8760h"
      }
    }
  }
}
EOF
}

function generate_ca_csr() {
  cat > ca-csr.json << EOF
{
  "CN": "Kubernetes",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "Kubernetes",
      "OU": "CA",
      "ST": "Oregon"
    }
  ]
}
EOF

cfssl gencert -initca ca-csr.json | cfssljson -bare ca

}

function generate_certificate_pair() {
    user="$1"
    echo -e "\n\nGenerating Certificate Pair for ${user}\n"
    cat > "${user}"-csr.json << EOF
{
  "CN": "${user}",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "Kubernetes",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

    cfssl gencert \
        -ca=ca.pem \
        -ca-key=ca-key.pem \
        -config=ca-config.json \
        -profile=kubernetes \
        "${user}"-csr.json | cfssljson -bare "${user}"

}

function generate_certificate_pair_with_hostname() {
node=$1
external_ip=$2
internal_ip=$3

echo -e "\n\nGenerating Certificate Pair for ${node} with hostnames ${external_ip}, ${internal_ip}\n"

cat > "${node}"-csr.json << EOF
{
  "CN": "system:node:${node}",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "system:nodes",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

    cfssl gencert \
        -ca=ca.pem \
        -ca-key=ca-key.pem \
        -config=ca-config.json \
        -hostname="${node}","${external_ip}","${internal_ip}", 127.0.0.1, kubernetes.default \
        -profile=kubernetes \
        "${node}"-csr.json | cfssljson -bare "${node}"
}

generate_ca_config
generate_ca_csr
generate_certificate_pair "service-account"
generate_certificate_pair "admin"
generate_certificate_pair "kube-proxy"
generate_certificate_pair "kube-scheduler"
generate_certificate_pair "kube-controller-manger"
generate_certificate_pair_with_hostname "worker-0" "192.168.1.254" "192.168.1.254"
generate_certificate_pair_with_hostname "kubernetes" "192.168.1.254" "192.168.1.254"
