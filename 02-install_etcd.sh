#!/bin/bash -e

ETCD_VER="v3.3.13"
INSTALL_DIR="/tmp"

echo -e "Installing etcd version ${ETCD_VER} to ${INSTALL_DIR}..."

# choose either URL
GOOGLE_URL=https://storage.googleapis.com/etcd
# GITHUB_URL=https://github.com/etcd-io/etcd/releases/download
DOWNLOAD_URL=${GOOGLE_URL}

echo -e "Removing existing etcd package from ${INSTALL_DIR}..."
rm -f ${INSTALL_DIR}/etcd-${ETCD_VER}-linux-amd64.tar.gz
rm -rf ${INSTALL_DIR}/etcd-download-test && mkdir -p ${INSTALL_DIR}/etcd-download-test

echo -e "Downloading etcd ${ETCD_VER} from ${DOWNLOAD_URL}..."
curl -L ${DOWNLOAD_URL}/${ETCD_VER}/etcd-${ETCD_VER}-linux-amd64.tar.gz -o ${INSTALL_DIR}/etcd-${ETCD_VER}-linux-amd64.tar.gz
tar xzvf ${INSTALL_DIR}/etcd-${ETCD_VER}-linux-amd64.tar.gz -C ${INSTALL_DIR}/etcd-download-test --strip-components=1
rm -f ${INSTALL_DIR}/etcd-${ETCD_VER}-linux-amd64.tar.gz

${INSTALL_DIR}/etcd-download-test/etcd --version
ETCDCTL_API=3 ${INSTALL_DIR}/etcd-download-test/etcdctl version
