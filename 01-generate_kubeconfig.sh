#!/bin/bash -e

echo "Please enter cluster name:"
read -r CLUSTER_NAME
if [ -z "${CLUSTER_NAME}" ];
then
  echo -e "Cluster name was not specified.  Please specify a cluster name"
  exit 1
fi

echo "Please enter controller server IP:"
read -r SERVER_IP

if [[ "${SERVER_IP}" =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
    IFS='.' ip_array=(${SERVER_IP[@]})
    [[ 10#${ip_array[0]} -le 255 && 10#${ip_array[1]} -le 255 && 10#${ip_array[2]} -le 255 && 10#${ip_array[3]} -le 255 ]]
    stat=$?
    if [[ "${stat}" != 0 ]]; then
      echo "Invalid IP found: ${SERVER_IP}.  Please specify a valid IP"
      exit "${stat}"
    fi
else
  echo "Invalid IP found: ${SERVER_IP}.  Please specify a valid IP"
  exit 1
fi

echo "Please enter Kubernetes client name (Node, User, Service. eg. kube-proxy):"
read -r CLIENT_NAME
if [ -z "${CLIENT_NAME}" ];
then
  echo -e "Client name was not specified.  Please specify a client name"
  exit 1

fi

echo -e "Please enter cluster role for client (eg. system:, system:node:, leave blank for none)"
read -r CLUSTER_ROLE

if [[ -z "${CLUSTER_ROLE}" ]]; then
  echo -e "Cluster role not specified, assuming none\n"
fi


echo "Cluster Name: ${CLUSTER_NAME}"
echo "Controller Server IP: ${SERVER_IP}"
echo "Client to be configured: ${CLIENT_NAME}"
echo "User specified for client ${CLIENT_NAME}: ${CLUSTER_ROLE}${CLIENT_NAME}"

read -p "Continue with creating kubeconfig file for ${CLIENT_NAME}? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]];
  then
    echo "Generating kubeconfig for ${CLIENT_NAME}"
    kubectl config set-cluster "${CLUSTER_NAME}" \
      --certificate-authority=ca.pem \
      --embed-certs=true \
      --server=https://"${SERVER_IP}":6443 \
      --kubeconfig="${CLIENT_NAME}".kubeconfig

    kubectl config set-credentials "${CLUSTER_ROLE}""${CLIENT_NAME}" \
      --client-certificate="${CLIENT_NAME}".pem \
      --client-key="${CLIENT_NAME}"-key.pem \
      --embed-certs=true \
      --kubeconfig="${CLIENT_NAME}".kubeconfig

    kubectl config set-context default \
      --cluster="${CLUSTER_NAME}" \
      --user="${CLUSTER_ROLE}""${CLIENT_NAME}" \
      --kubeconfig="${CLIENT_NAME}".kubeconfig

    kubectl config use-context default \
      --kubeconfig="${CLIENT_NAME}".kubeconfig
    fi
