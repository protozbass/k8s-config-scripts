#!/bin/bash -e

# Updated 26Nov2019
CRI_TOOLS="v1.16.1"
K8S_VERSION="v1.16.3"
RUNC_VERSION="v1.0.0-rc9"
CNI_VERSION="v0.8.3"
CONTAINERD_VERSION="1.3.1"
COREDNS_VERSION="1.6.5"

wget -q --show-progress --https-only --timestamping \
	https://github.com/kubernetes-sigs/cri-tools/releases/download/${CRI_TOOLS}/crictl-${CRI_TOOLS}-linux-amd64.tar.gz \
	https://storage.googleapis.com/gvisor/releases/nightly/latest/runsc \
	https://github.com/opencontainers/runc/releases/download/${RUNC_VERSION}/runc.amd64 \
	https://github.com/containernetworking/plugins/releases/download/${CNI_VERSION}/cni-plugins-amd64-${CNI_VERSION}.tgz \
	https://github.com/containerd/containerd/releases/download/v${CONTAINERD_VERSION}/containerd-${CONTAINERD_VERSION}.linux-amd64.tar.gz \
	https://storage.googleapis.com/kubernetes-release/release/${K8S_VERSION}/bin/linux/amd64/kubectl \
	https://storage.googleapis.com/kubernetes-release/release/${K8S_VERSION}/bin/linux/amd64/kube-proxy \
	https://storage.googleapis.com/kubernetes-release/release/${K8S_VERSION}/bin/linux/amd64/kubelet
  https://storage.googleapis.com/kubernetes-release/release/${K8S_VERSION}/bin/linux/amd64/kube-controller-manager
  https://storage.googleapis.com/kubernetes-release/release/${K8S_VERSION}/bin/linux/amd64/kube-apiserver
  https://storage.googleapis.com/kubernetes-release/release/${K8S_VERSION}/bin/linux/amd64/kube-scheduler
  https://github.com/coredns/coredns/releases/download/v${COREDNS_VERSION}/coredns_${COREDNS_VERSION}_linux_amd64.tgz

sudo mkdir -p \
  /etc/cni/net.d \
  /opt/cni/bin \
  /var/lib/kubelet \
  /var/lib/kube-proxy \
  /var/lib/kubernetes \
  /var/run/kubernetes

  {
  mkdir containerd
  tar -xvf crictl-${K8S_VERSION}-linux-amd64.tar.gz
  tar -xvf containerd-${CONTAINERD_VERSION}.linux-amd64.tar.gz -C containerd
  sudo tar -xvf cni-plugins-linux-amd64-${CNI_VERSION}.tgz -C /opt/cni/bin/
  sudo mv runc.amd64 runc
  chmod +x crictl kubectl kube-proxy kubelet runc kube-apiserver kube-scheduler kube-controller-manager
  sudo mv crictl kubectl kube-proxy kubelet runc kube-apiserver kube-scheduler kube-controller-manager /usr/local/bin/
  sudo mv containerd/bin/* /bin/
}